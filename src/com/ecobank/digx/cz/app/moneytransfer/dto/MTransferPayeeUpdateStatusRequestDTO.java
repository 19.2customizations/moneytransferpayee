package com.ecobank.digx.cz.app.moneytransfer.dto;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.enumeration.payment.payee.PayeeStatusType;

public class MTransferPayeeUpdateStatusRequestDTO extends DataTransferObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String payeeId;
	private String groupId;
	private String partyId;
	private PayeeStatusType status;

	public String getPayeeId() {
		return this.payeeId;
	}

	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}

	public String getGroupId() {
		return this.groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getPartyId() {
		return this.partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public PayeeStatusType getStatus() {
		return this.status;
	}

	public void setStatus(PayeeStatusType status) {
		this.status = status;
	}
}
