package com.ecobank.digx.cz.app.moneytransfer.dto;

import com.ofss.digx.service.response.BaseResponseObject;

public class ResponseDTO extends BaseResponseObject {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private String payeeId;
	private String responseCode;
	private String responseMessage;
	public String getPayeeId() {
		return payeeId;
	}
	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
