package com.ecobank.digx.cz.app.domain.moneytransfer.entity.repository.adapter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.ecobank.digx.cz.app.domain.moneytransfer.entity.payee.CzMoneyTransferPayee;
import com.ofss.digx.domain.payment.entity.payee.PayeeKey;
import com.ofss.digx.enumeration.payment.payee.PayeeStatusType;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.digx.framework.domain.repository.adapter.AbstractLocalRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public class LocalMoneyTransferPayeeRepositoryAdapter extends AbstractLocalRepositoryAdapter<CzMoneyTransferPayee>
		implements IMoneyTransferPayeeRepositoryAdapter {

	private static LocalMoneyTransferPayeeRepositoryAdapter singletonInstance;

	public static LocalMoneyTransferPayeeRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (LocalMoneyTransferPayeeRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new LocalMoneyTransferPayeeRepositoryAdapter();
			}
		return singletonInstance;
	}

	public void create(CzMoneyTransferPayee MoneyTransferPayee) throws Exception {
		if (MoneyTransferPayee.getKey() != null)
			MoneyTransferPayee.getKey().setDeterminantValue(
					DeterminantResolver.getInstance().fetchDeterminantValue(CzMoneyTransferPayee.class.getName()));
		insert(MoneyTransferPayee);
	}

	public void update(CzMoneyTransferPayee MoneyTransferPayee) throws Exception {
		if (MoneyTransferPayee.getKey() != null && MoneyTransferPayee.getKey().getDeterminantValue() == null)
			MoneyTransferPayee.getKey().setDeterminantValue(
					DeterminantResolver.getInstance().fetchDeterminantValue(CzMoneyTransferPayee.class.getName()));
		super.update(MoneyTransferPayee);
	}

	@Override
	public List<CzMoneyTransferPayee> list(PayeeKey key, String groupId, String nickName) throws Exception {
		HashMap<String, Object> parameters = null;
		List<CzMoneyTransferPayee> MoneyTransferPayeeList = null;
		parameters = new HashMap<>();
		parameters.put("partyid", key.getPartyId());
		parameters.put("determinantvalue",
				DeterminantResolver.getInstance().fetchDeterminantValue(CzMoneyTransferPayee.class.getName()));
		//parameters.put("groupId", groupId);
		/*if (nickName != null) {
			parameters.put("nickName", "%" + nickName.toLowerCase() + "%");
		} else {
			parameters.put("nickName", "%");
		}*/
		//parameters.put("partyType", groupId);
		parameters.put("status", PayeeStatusType.ACTIVE);
		MoneyTransferPayeeList = executeNamedQuery("ListMoneyTransferPayeeByPartyId", parameters);
		return MoneyTransferPayeeList;
	}
	
	
	
	@Override
	public List<CzMoneyTransferPayee> listAll(String partyId, String transferPayeeType,String nickname) throws Exception {
		HashMap<String, Object> parameters = null;
		List<CzMoneyTransferPayee> MoneyTransferPayeeList = null;
		parameters = new HashMap<>();
		parameters.put("partyid", partyId);
		String queryName = "ListMoneyTransferPayeeAll";
		if(transferPayeeType != null && !transferPayeeType.equals(""))
		{
			queryName = "ListMoneyTransferPayeeAllbyPayeeType";
			parameters.put("transferPayeeType", transferPayeeType);
		}
		else if(nickname != null && !nickname.equals(""))
		{
			queryName = "ListMoneyTransferPayeeAllbyName";
			parameters.put("nickname", nickname);
		}
		
		System.out.println("FETCH-PAYEE-REMIT: " + queryName + " " + transferPayeeType);
		
		//parameters.put("status", PayeeStatusType.ACTIVE.toString());
		MoneyTransferPayeeList = executeNamedQuery(queryName, parameters);
		return MoneyTransferPayeeList;
	}
	
	@Override
	public boolean payeeDuplicateCheck(String partyId, String receiveMode, String transferPayeeType,String nickname, String accountNo ) throws Exception {
		HashMap<String, Object> parameters = null;
		boolean result = false;
		List<CzMoneyTransferPayee> moneyTransferPayeeList = null;
		parameters = new HashMap<>();
		parameters.put("partyid", partyId);
		parameters.put("receiveMethod", receiveMode);
		parameters.put("transferPayeeType", transferPayeeType);
		String queryName = "CheckMoneyTransferPayeeExistByCash";
		if(receiveMode != null && receiveMode.equals("CASH"))
		{
			queryName = "CheckMoneyTransferPayeeExistByCash";
			parameters.put("nickname", nickname);
		}
		else
		{
			queryName = "CheckMoneyTransferPayeeExistByAccount";
			parameters.put("accountNo", accountNo);
		}
		
		System.out.println("Duplicate Check Payee: " + queryName);
		
		//parameters.put("status", PayeeStatusType.ACTIVE);
		moneyTransferPayeeList = executeNamedQuery(queryName, parameters);
		if(moneyTransferPayeeList != null && moneyTransferPayeeList.size() > 0)
			result = true;
		else
			result = false;
		
		return result;
	}

	@Override
	public CzMoneyTransferPayee read(PayeeKey key) throws Exception {
		key.setDeterminantValue(
				DeterminantResolver.getInstance().fetchDeterminantValue(CzMoneyTransferPayee.class.getName()));
		return (CzMoneyTransferPayee) get(CzMoneyTransferPayee.class, (Serializable) key);
	}

	public void delete(CzMoneyTransferPayee MoneyTransferPayee) throws Exception {
		if (MoneyTransferPayee.getKey() != null && MoneyTransferPayee.getKey().getDeterminantValue() == null)
			MoneyTransferPayee.getKey().setDeterminantValue(
					DeterminantResolver.getInstance().fetchDeterminantValue(CzMoneyTransferPayee.class.getName()));
		super.delete(MoneyTransferPayee);
	}

}
