package com.ecobank.digx.cz.app.moneytransfer.service;


import com.ecobank.digx.cz.app.moneytransfer.dto.MTransferPayeeUpdateStatusRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MTransferPayeeUpdateStatusResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeListRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeListResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.PayeeStatusRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.PayeeStatusResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.ResponseDTO;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.app.context.SessionContext;

public interface IMoneyTransferPayee {
	
	 ResponseDTO create(SessionContext sessionContext,MoneyTransferPayeeRequestDTO requestDTO) throws Exception ;

	MoneyTransferPayeeListResponse list(SessionContext paramSessionContext,
			MoneyTransferPayeeListRequestDTO paramMoneyTransferPayeeListRequestDTO) throws Exception;

	MTransferPayeeUpdateStatusResponse updateStatus(SessionContext paramSessionContext,
			MTransferPayeeUpdateStatusRequestDTO paramMTransferPayeeUpdateStatusRequestDTO) throws Exception;

	PayeeStatusResponse delete(SessionContext sessionContext, PayeeStatusRequestDTO requestDTO) throws Exception; 
}
