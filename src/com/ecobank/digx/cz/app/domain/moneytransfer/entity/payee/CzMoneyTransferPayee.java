package com.ecobank.digx.cz.app.domain.moneytransfer.entity.payee;

import java.util.List;

import com.ecobank.digx.cz.app.domain.moneytransfer.entity.repository.MoneyTransferPayeeRepository;
import com.ofss.digx.domain.payment.entity.payee.Payee;
import com.ofss.digx.domain.payment.entity.payee.PayeeKey;
import com.ofss.digx.infra.exceptions.Exception;

public class CzMoneyTransferPayee extends Payee {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MoneyTransferPayeeDetails moneyTransferPayeeDetails;

	public MoneyTransferPayeeDetails getMoneyTransferPayeeDetails() {
		return this.moneyTransferPayeeDetails;
	}

	public void setMoneyTransferPayeeDetails(MoneyTransferPayeeDetails moneyTransferPayeeDetails) {
		this.moneyTransferPayeeDetails = moneyTransferPayeeDetails;
	}

	public void create(CzMoneyTransferPayee moneyTransferPayeeDomainObject) throws Exception {
		MoneyTransferPayeeRepository moneyTransferPayeeRepository = MoneyTransferPayeeRepository.getInstance();
		moneyTransferPayeeRepository.create(moneyTransferPayeeDomainObject);
	}

	public void update(CzMoneyTransferPayee internalPayee) throws Exception {
		MoneyTransferPayeeRepository moneyTransferPayeeRepository = MoneyTransferPayeeRepository.getInstance();
		moneyTransferPayeeRepository.update(internalPayee);
	}

	public List<CzMoneyTransferPayee> list(PayeeKey key, String groupId, String nickName) throws Exception {
		MoneyTransferPayeeRepository moneyTransferPayeeRepository = MoneyTransferPayeeRepository.getInstance();
		List<CzMoneyTransferPayee> moneyTransferPayeeList = moneyTransferPayeeRepository.list(key, groupId, nickName);
		return moneyTransferPayeeList;
	}
	
	public List<CzMoneyTransferPayee> listAll(String partyId, String transferPayeeType,String nickname) throws Exception 
	{
		MoneyTransferPayeeRepository moneyTransferPayeeRepository = MoneyTransferPayeeRepository.getInstance();
		List<CzMoneyTransferPayee> moneyTransferPayeeList = moneyTransferPayeeRepository.listAll(partyId, transferPayeeType, nickname);
		return moneyTransferPayeeList;
   }
	
	public boolean payeeDuplicateCheck(String partyId, String receiveMode, String transferPayeeType,String nickname, String accountNo ) throws Exception {
		
		MoneyTransferPayeeRepository moneyTransferPayeeRepository = MoneyTransferPayeeRepository.getInstance();
		return moneyTransferPayeeRepository.payeeDuplicateCheck(partyId, receiveMode, transferPayeeType, nickname, accountNo);
	}

	public CzMoneyTransferPayee read(PayeeKey key) throws Exception {
		MoneyTransferPayeeRepository moneyTransferPayeeRepository = MoneyTransferPayeeRepository.getInstance();
		CzMoneyTransferPayee moneyTransferPayee = moneyTransferPayeeRepository.read(key);
		return moneyTransferPayee;
	}

	public void delete(CzMoneyTransferPayee moneyTransferPayee) throws Exception {
		MoneyTransferPayeeRepository moneyTransferPayeeRepository = MoneyTransferPayeeRepository.getInstance();
		moneyTransferPayeeRepository.delete(moneyTransferPayee);
	}


}
