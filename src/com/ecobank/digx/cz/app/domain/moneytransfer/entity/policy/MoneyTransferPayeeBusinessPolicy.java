package com.ecobank.digx.cz.app.domain.moneytransfer.entity.policy;

import java.util.prefs.Preferences;


import com.ecobank.digx.cz.app.domain.moneytransfer.entity.payee.CzMoneyTransferPayee;
import com.ofss.digx.domain.payment.entity.policy.AbstractPaymentBusinessPolicy;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.framework.domain.policy.IBusinessPolicyDTO;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.infra.validation.error.ValidationError;

public class MoneyTransferPayeeBusinessPolicy extends AbstractPaymentBusinessPolicy {
	  private static final String THIS_COMPONENT_NAME = MoneyTransferPayeeBusinessPolicy.class.getName();
	  
	  private static final MultiEntityLogger FORMATTER = MultiEntityLogger.getUniqueInstance();
	  
	  //private static final Logger logger = FORMATTER.getLogger(THIS_COMPONENT_NAME);
	  
	  private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");
	  
	  private MoneyTransferPayeeBusinessPolicyData policyData;
	  
	  public MoneyTransferPayeeBusinessPolicy() {}
	  
	  public MoneyTransferPayeeBusinessPolicy(IBusinessPolicyDTO iBusinessPolicyDTO) {
	    if (iBusinessPolicyDTO instanceof MoneyTransferPayeeBusinessPolicyData) {
	      this.policyData = (MoneyTransferPayeeBusinessPolicyData)iBusinessPolicyDTO;
	      
	      
	    } 
	  }
	  
	  public void validatePolicy() {
		  
		  
		 
		  System.out.println("MoneyTransferPayeeBusinessPolicy222  : " + policyData.getPartyId());
		  System.out.println("MoneyTransferPayeeBusinessPolicy AffCode: " + policyData.getReceiveMode());
	
		  
		 //Check duplicate
		  

			CzMoneyTransferPayee payeeDomain = new CzMoneyTransferPayee();
			
			boolean isDuplicated;
			try {
				isDuplicated = payeeDomain.payeeDuplicateCheck(policyData.getPartyId(), policyData.getReceiveMode(), policyData.getTransferPayeeType(), policyData.getNickname(), policyData.getAccountNo());
				
				System.out.println("Duplicate Check : " + isDuplicated);
				if(isDuplicated)
				{
					addValidationError(new ValidationError("MoneyTransferPayeeBusinessPolicy", "beneficiaryCcy", null, "DIGX_CZ_0006", new String[] { "" }));
				    return;
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		  
		  
		  
		  
	  }

}
