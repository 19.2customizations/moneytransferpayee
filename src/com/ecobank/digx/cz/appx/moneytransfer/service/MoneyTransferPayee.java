package com.ecobank.digx.cz.appx.moneytransfer.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.ecobank.digx.cz.app.moneytransfer.dto.MTransferPayeeUpdateStatusRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MTransferPayeeUpdateStatusResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeListRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeListResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.PayeeStatusRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.PayeeStatusResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.ResponseDTO;
import com.ecobank.digx.cz.app.moneytransfer.service.MoneyTransferPayeeService;
import com.ofss.digx.app.context.ChannelContext;
import com.ofss.digx.app.core.ChannelInteraction;
import com.ofss.digx.appx.AbstractRESTApplication;
import com.ofss.digx.appx.PATCH;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.digx.service.response.BaseResponseObject;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.service.response.TransactionStatus;


@Path("/czpayee")
public class MoneyTransferPayee extends AbstractRESTApplication implements IMoneyTransferPayee {

	private static final String THIS_COMPONENT_NAME = MoneyTransferPayee.class.getName();
	private static final MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	private static final Logger logger = formatter.getLogger(THIS_COMPONENT_NAME);

	private String groupId;

	public String getGroupId() {
		return this.groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public void MoneyTransferPayeeyyy(String groupId) {
		this.groupId = groupId;
	}
	
	
	@Override
	@POST
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	public Response create(MoneyTransferPayeeRequestDTO requestDTO) {
		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE,
					formatter.formatMessage(
							"Entered into create method of Money Transfer Payee REST Service.  Input: CzMoneyTransferPayee: %s",
							new Object[] { requestDTO }));
		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		ResponseDTO createResponse = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			
			MoneyTransferPayeeService service = new MoneyTransferPayeeService();
			createResponse = service.create(channelContext.getSessionContext(), requestDTO);
			response = buildResponse((BaseResponseObject) createResponse, Response.Status.CREATED);
		} catch (Exception e) {
			logger.log(Level.SEVERE,
					formatter.formatMessage(
							"Exception encountered while invoking the core service create for CZ Money Transfer Payee=%s",
							new Object[] { requestDTO }),
					(Throwable) e);
			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while closing channelContext %s",
						new Object[] { channelContext }), (Throwable) e);
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, formatter.formatMessage("Exiting from create() : createResponse=%s",
					new Object[] { createResponse }));
		return response;
	}

	
	@GET
	@Produces({ "application/json" })
	public Response list(@QueryParam("type") String type, @QueryParam("nickName") String nickName) {
		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE,
					formatter.formatMessage("Entered into list of MoneyTransferPayee REST Service.", new Object[0]));
		ChannelContext channelContext = null;
		ChannelInteraction channelInteraction = null;
		Response response = null;
		MoneyTransferPayeeListResponse responseDTO = null;
		MoneyTransferPayeeListRequestDTO payeeRequest = new MoneyTransferPayeeListRequestDTO();
		MoneyTransferPayeeService payeeService = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			/*if (!isNullOrBlank(types)) {
				String[] typeArray = types.split(",");
				List<MoneyTransferPayeeType> typeList = new ArrayList<>();
				for (int i = 0; i < typeArray.length; i++) {
					if (MoneyTransferPayeeType.fromValue(typeArray[i]) == null) {
						response = buildResponse((BaseResponseObject) responseDTO, Response.Status.BAD_REQUEST);
						return response;
					}
					typeList.add(MoneyTransferPayeeType.fromValue(typeArray[i]));
				}
				payeeRequest.setTypes(typeList);
			}*/
			channelInteraction.begin(channelContext);
			payeeRequest.setNickName(nickName);
			//payeeRequest.setGroupId(getGroupId());
			payeeRequest.setPayeeTransferType(type);
			payeeService = new MoneyTransferPayeeService();
			responseDTO = payeeService.list(channelContext.getSessionContext(), payeeRequest);
			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE,
					formatter.formatMessage("Exception encountered while invoking the list service  %s, ",
							new Object[] { THIS_COMPONENT_NAME, e }));
			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while closing channelContext %s",
						new Object[] { channelContext }), (Throwable) e);
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, formatter.formatMessage(
					"Exiting from list of MoneyTransferPayee REST Service. Output: MoneyTransferPayeeListResponse %s",
					new Object[] { responseDTO }));
		return response;
	}

	@GET
	@Path("/listbytransfertype")
	@Produces({ "application/json" })
	public Response listAll(@QueryParam("payeeTransferType") String payeeTransferType, @QueryParam("nickName") String nickName) {
		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE,
					formatter.formatMessage("Entered into list of MoneyTransferPayee REST Service.", new Object[0]));
		ChannelContext channelContext = null;
		ChannelInteraction channelInteraction = null;
		Response response = null;
		MoneyTransferPayeeListResponse responseDTO = null;
		MoneyTransferPayeeListRequestDTO payeeRequest = new MoneyTransferPayeeListRequestDTO();
		MoneyTransferPayeeService payeeService = new MoneyTransferPayeeService();
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			/*if (!isNullOrBlank(types)) {
				String[] typeArray = types.split(",");
				List<MoneyTransferPayeeType> typeList = new ArrayList<>();
				for (int i = 0; i < typeArray.length; i++) {
					if (MoneyTransferPayeeType.fromValue(typeArray[i]) == null) {
						response = buildResponse((BaseResponseObject) responseDTO, Response.Status.BAD_REQUEST);
						return response;
					}
					typeList.add(MoneyTransferPayeeType.fromValue(typeArray[i]));
				}
				payeeRequest.setTypes(typeList);
			}*/
			channelInteraction.begin(channelContext);
			payeeRequest.setNickName(nickName);
			payeeRequest.setPayeeTransferType(payeeTransferType);
			//payeeRequest.setGroupId(getGroupId());
			
			payeeService = new MoneyTransferPayeeService();
			responseDTO = payeeService.list(channelContext.getSessionContext(), payeeRequest);
			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE,
					formatter.formatMessage("Exception encountered while invoking the list service  %s, ",
							new Object[] { THIS_COMPONENT_NAME, e }));
			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while closing channelContext %s",
						new Object[] { channelContext }), (Throwable) e);
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, formatter.formatMessage(
					"Exiting from list of MoneyTransferPayee REST Service. Output: MoneyTransferPayeeListResponse %s",
					new Object[] { responseDTO }));
		return response;
	}

	
	@PATCH
	@Path("/{payeeId}")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateStatus(@PathParam("payeeId") String payeeId) {
		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, formatter.formatMessage(
					"Entered into update method of Payee REST Service. Input: payeeId : %s", new Object[] { payeeId }));
		Response response = null;
		ChannelContext channelContext = null;
		ChannelInteraction channelInteraction = null;
		MTransferPayeeUpdateStatusResponse payeeUpdateStatusResponse = null;
		MTransferPayeeUpdateStatusRequestDTO payeeUpdateRequestDTO = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			payeeUpdateStatusResponse = new MTransferPayeeUpdateStatusResponse();
			MoneyTransferPayeeService payeeService = new MoneyTransferPayeeService();
			payeeUpdateRequestDTO = new MTransferPayeeUpdateStatusRequestDTO();
			payeeUpdateRequestDTO.setPayeeId(payeeId);
			payeeUpdateStatusResponse = payeeService.updateStatus(channelContext.getSessionContext(),
					payeeUpdateRequestDTO);
			response = buildResponse((BaseResponseObject) payeeUpdateStatusResponse, Response.Status.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE,
					formatter.formatMessage("Exception encountered while invoking the update service for payeeId : %s",
							new Object[] { payeeId, e }));
			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while closing channelContext %s",
						new Object[] { channelContext }), (Throwable) e);
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, formatter.formatMessage("Exiting from update of  payee REST Service. : payeeId : %s",
					new Object[] { payeeUpdateStatusResponse }));
		return response;
	}
	
	
	@Override
	@DELETE
	@Path("/{payeeId}")
	@Produces({ "application/json" })
	public Response delete(@PathParam("payeeId") String payeeId) {
		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE,
					formatter.formatMessage(
							"Entered into delete method of Rapid Transfer Payee REST Service. Input: payeeId : %s",
							new Object[] { payeeId }));
		Response response = null;
		ChannelContext channelContext = null;
		ChannelInteraction channelInteraction = null;
		TransactionStatus transactionStatus = null;
		MoneyTransferPayeeService payeeService = new MoneyTransferPayeeService();
		PayeeStatusRequestDTO requestDTO = new PayeeStatusRequestDTO();
		PayeeStatusResponse responseDTO = new PayeeStatusResponse();
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			
			requestDTO.setPayeeId(payeeId);
			
			responseDTO = payeeService.delete(channelContext.getSessionContext(), requestDTO);
			response = buildResponse(responseDTO, Response.Status.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE, formatter.formatMessage(
					"Exception encountered while invoking the delete service for InternalPayeeReadRequestDTO : %s",
					new Object[] { requestDTO, e }));
			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while closing channelContext %s",
						new Object[] { channelContext }), (Throwable) e);
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, formatter.formatMessage(
					"Exiting from delete of Rapid Transfer payee REST Service. RapidTransferPayeeRequestDTO : %s",
					new Object[] { transactionStatus }));
		return response;
	}

	private boolean isNullOrBlank(String string) {
		if (string == null || string.trim().equals(""))
			return true;
		return false;
	}

}
