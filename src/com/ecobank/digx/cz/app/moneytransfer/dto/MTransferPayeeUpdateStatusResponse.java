package com.ecobank.digx.cz.app.moneytransfer.dto;

import com.ofss.digx.service.response.BaseResponseObject;

public class MTransferPayeeUpdateStatusResponse extends BaseResponseObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean tokenAvailable;

	public boolean isTokenAvailable() {
		return this.tokenAvailable;
	}

	public void setTokenAvailable(boolean tokenAvailable) {
		this.tokenAvailable = tokenAvailable;
	}
}
