package com.ecobank.digx.cz.app.domain.moneytransfer.entity.repository.adapter;

import java.util.List;

import com.ecobank.digx.cz.app.domain.moneytransfer.entity.payee.CzMoneyTransferPayee;
import com.ofss.digx.domain.payment.entity.payee.PayeeKey;
import com.ofss.digx.framework.domain.repository.IRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public interface IMoneyTransferPayeeRepositoryAdapter extends IRepositoryAdapter<CzMoneyTransferPayee, PayeeKey> {
	void create(CzMoneyTransferPayee paramMoneyTransferPayee) throws Exception;

	void update(CzMoneyTransferPayee paramMoneyTransferPayee) throws Exception;

	List<CzMoneyTransferPayee> list(PayeeKey paramMoneyTransferPayeeKey, String paramString1,
			String paramString2) throws Exception;

	CzMoneyTransferPayee read(PayeeKey paramMoneyTransferPayeeKey) throws Exception;
	
	 List<CzMoneyTransferPayee> listAll(String partyId, String transferPayeeType,String nickname) throws Exception;

	void delete(CzMoneyTransferPayee paramMoneyTransferPayee) throws Exception;
	 boolean payeeDuplicateCheck(String partyId, String receiveMode, String transferPayeeType,String nickname, String accountNo ) throws Exception;
}
