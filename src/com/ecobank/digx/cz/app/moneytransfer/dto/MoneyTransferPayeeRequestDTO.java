package com.ecobank.digx.cz.app.moneytransfer.dto;

import java.util.logging.Logger;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class MoneyTransferPayeeRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = MoneyTransferPayeeRequestDTO.class.getName();
	private transient MultiEntityLogger formatter;
	
	private MoneyTransferPayeeDTO payeeDTO;

	public MoneyTransferPayeeRequestDTO() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}
	
	private transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);

	public MoneyTransferPayeeDTO getPayeeDTO() {
		return payeeDTO;
	}

	public void setPayeeDTO(MoneyTransferPayeeDTO payeeDTO) {
		this.payeeDTO = payeeDTO;
	}

	
	
}
