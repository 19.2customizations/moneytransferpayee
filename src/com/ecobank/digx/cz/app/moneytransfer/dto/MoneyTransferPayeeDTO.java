package com.ecobank.digx.cz.app.moneytransfer.dto;


import com.ecobank.digx.cz.app.enumeration.moneytransfer.payee.MoneyTransferPayeeType;
import com.ofss.digx.app.approval.dto.transaction.TransactionUserDetailsDTO;
import com.ofss.digx.app.common.dto.DomainObjectDTO;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.digx.enumeration.payment.payee.PayeeAccessType;
import com.ofss.digx.enumeration.payment.payee.PayeeStatusType;

import io.swagger.v3.oas.annotations.media.Schema;

public class MoneyTransferPayeeDTO extends DomainObjectDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TransactionUserDetailsDTO userDetails;
	private String id;
	private String partyId;
	private String groupId;
	private String groupName;
	private String phoneNo;
	private String email;
	private String nickName;
	private MoneyTransferPayeeType payeeType;
	private String destCountry;
	private String origCountry;
	private PayeeStatusType status;
	private String tokenId;
	private Boolean isShared;
	private PayeeAccessType payeeAccessType;
	private boolean adhocPayeeFlag;
	
	private String payeeTransferType;

	private String firstName;
	private String lastName;
	private String accountId;
	private String accountName;
	private String accountType;
	private String bankCode;
	private String receiveMode;
	private String idType;
	private String idNumber;
	private String idIssueDate;
	private String idExpiryDate;
	private String otherIdName;
	private String destinationBank;
	private String country;
	private String ccyCode;
	
	
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	
	@Schema(description = "Account Id", type = "Account", required = true)
	private Account sourceAccountId;
	
	
	

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getReceiveMode() {
		return receiveMode;
	}

	public void setReceiveMode(String receiveMode) {
		this.receiveMode = receiveMode;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdIssueDate() {
		return idIssueDate;
	}

	public void setIdIssueDate(String idIssueDate) {
		this.idIssueDate = idIssueDate;
	}

	public String getIdExpiryDate() {
		return idExpiryDate;
	}

	public void setIdExpiryDate(String idExpiryDate) {
		this.idExpiryDate = idExpiryDate;
	}

	public String getOtherIdName() {
		return otherIdName;
	}

	public void setOtherIdName(String otherIdName) {
		this.otherIdName = otherIdName;
	}

	public String getDestinationBank() {
		return destinationBank;
	}

	public void setDestinationBank(String destinationBank) {
		this.destinationBank = destinationBank;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCcyCode() {
		return ccyCode;
	}

	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public TransactionUserDetailsDTO getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(TransactionUserDetailsDTO userDetails) {
		this.userDetails = userDetails;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public MoneyTransferPayeeType getPayeeType() {
		return payeeType;
	}

	public void setPayeeType(MoneyTransferPayeeType payeeType) {
		this.payeeType = payeeType;
	}

	public String getDestCountry() {
		return destCountry;
	}

	public void setDestCountry(String destCountry) {
		this.destCountry = destCountry;
	}

	public String getOrigCountry() {
		return origCountry;
	}

	public void setOrigCountry(String origCountry) {
		this.origCountry = origCountry;
	}

	public PayeeStatusType getStatus() {
		return status;
	}

	public void setStatus(PayeeStatusType status) {
		this.status = status;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public Boolean getIsShared() {
		return isShared;
	}

	public void setIsShared(Boolean isShared) {
		this.isShared = isShared;
	}

	public PayeeAccessType getPayeeAccessType() {
		return payeeAccessType;
	}

	public void setPayeeAccessType(PayeeAccessType payeeAccessType) {
		this.payeeAccessType = payeeAccessType;
	}

	public boolean isAdhocPayeeFlag() {
		return adhocPayeeFlag;
	}

	public void setAdhocPayeeFlag(boolean adhocPayeeFlag) {
		this.adhocPayeeFlag = adhocPayeeFlag;
	}

	@Override
	public String toString() {
		return "MoneyTransferPayeeDTO [userDetails=" + userDetails + ", id=" + id + ", partyId=" + partyId
				+ ", groupId=" + groupId + ", groupName=" + groupName + ", phoneNo=" + phoneNo + ", email=" + email
				+ ", nickName=" + nickName + ", payeeType=" + payeeType + ", destCountry=" + destCountry
				+ ", origCountry=" + origCountry + ", status=" + status + ", tokenId=" + tokenId + ", isShared="
				+ isShared + ", payeeAccessType=" + payeeAccessType + ", adhocPayeeFlag=" + adhocPayeeFlag + "]";
	}

	public String getPayeeTransferType() {
		return payeeTransferType;
	}

	public void setPayeeTransferType(String payeeTransferType) {
		this.payeeTransferType = payeeTransferType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

}
