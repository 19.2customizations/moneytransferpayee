package com.ecobank.digx.cz.app.domain.moneytransfer.entity.policy;

import com.ofss.digx.app.config.dto.workingwindow.WorkingWindowCheckResponse;
import com.ofss.fc.framework.domain.policy.IBusinessPolicyDTO;

public class MoneyTransferPayeeBusinessPolicyData implements IBusinessPolicyDTO {
	
	
	private WorkingWindowCheckResponse workingWindowCheckResponse;
	
	  private String partyId;
	  private String receiveMode;
	  private String transferPayeeType;
	  private String nickname;
	  private String accountNo;
	  
	  
	  
	public WorkingWindowCheckResponse getWorkingWindowCheckResponse() {
		return workingWindowCheckResponse;
	}
	public void setWorkingWindowCheckResponse(WorkingWindowCheckResponse workingWindowCheckResponse) {
		this.workingWindowCheckResponse = workingWindowCheckResponse;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getReceiveMode() {
		return receiveMode;
	}
	public void setReceiveMode(String receiveMode) {
		this.receiveMode = receiveMode;
	}
	public String getTransferPayeeType() {
		return transferPayeeType;
	}
	public void setTransferPayeeType(String transferPayeeType) {
		this.transferPayeeType = transferPayeeType;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	
	  

}
