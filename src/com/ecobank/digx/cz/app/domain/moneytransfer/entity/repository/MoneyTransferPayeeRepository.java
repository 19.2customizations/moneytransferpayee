package com.ecobank.digx.cz.app.domain.moneytransfer.entity.repository;

import java.util.List;

import com.ecobank.digx.cz.app.domain.moneytransfer.entity.payee.CzMoneyTransferPayee;
import com.ecobank.digx.cz.app.domain.moneytransfer.entity.repository.adapter.IMoneyTransferPayeeRepositoryAdapter;
import com.ofss.digx.domain.payment.entity.payee.PayeeKey;
import com.ofss.digx.framework.domain.repository.AbstractDomainObjectRepository;
import com.ofss.digx.framework.domain.repository.RepositoryAdapterFactory;
import com.ofss.digx.infra.exceptions.Exception;

public class MoneyTransferPayeeRepository
		extends AbstractDomainObjectRepository<CzMoneyTransferPayee, PayeeKey> {
	private static MoneyTransferPayeeRepository singletonInstance;

	public static MoneyTransferPayeeRepository getInstance() {
		if (singletonInstance == null)
			synchronized (MoneyTransferPayeeRepository.class) {
				if (singletonInstance == null)
					singletonInstance = new MoneyTransferPayeeRepository();
			}
		return singletonInstance;
	}

	public CzMoneyTransferPayee read(PayeeKey key) throws Exception {
		IMoneyTransferPayeeRepositoryAdapter repositoryAdapter = (IMoneyTransferPayeeRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("MONEY_TRANSFER_PAYEE_REPOSITORY_ADAPTER");
		CzMoneyTransferPayee MoneyTransferPayee = repositoryAdapter.read(key);
		return MoneyTransferPayee;
	}

	public void create(CzMoneyTransferPayee MoneyTransferPayee) throws Exception {
		IMoneyTransferPayeeRepositoryAdapter repositoryAdapter = (IMoneyTransferPayeeRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("MONEY_TRANSFER_PAYEE_REPOSITORY_ADAPTER");
		repositoryAdapter.create(MoneyTransferPayee);
	}

	public void update(CzMoneyTransferPayee MoneyTransferPayee) throws Exception {
		IMoneyTransferPayeeRepositoryAdapter repositoryAdapter = (IMoneyTransferPayeeRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("MONEY_TRANSFER_PAYEE_REPOSITORY_ADAPTER");
		repositoryAdapter.update(MoneyTransferPayee);
	}

	public void delete(CzMoneyTransferPayee MoneyTransferPayee) throws Exception {
		IMoneyTransferPayeeRepositoryAdapter repositoryAdapter = (IMoneyTransferPayeeRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("MONEY_TRANSFER_PAYEE_REPOSITORY_ADAPTER");
		repositoryAdapter.delete(MoneyTransferPayee);
	}

	public List<CzMoneyTransferPayee> list(PayeeKey key, String groupId, String nickName) throws Exception {
		IMoneyTransferPayeeRepositoryAdapter repositoryAdapter = (IMoneyTransferPayeeRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("MONEY_TRANSFER_PAYEE_REPOSITORY_ADAPTER");
		return repositoryAdapter.list(key, groupId, nickName);
	}
	
	public boolean payeeDuplicateCheck(String partyId, String receiveMode, String transferPayeeType,String nickname, String accountNo ) throws Exception {
		
		IMoneyTransferPayeeRepositoryAdapter repositoryAdapter = (IMoneyTransferPayeeRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("MONEY_TRANSFER_PAYEE_REPOSITORY_ADAPTER");
		return repositoryAdapter.payeeDuplicateCheck(partyId, receiveMode, transferPayeeType, nickname, accountNo);
	}
	
	
	public List<CzMoneyTransferPayee> listAll(String partyId, String transferPayeeType,String nickname) throws Exception 
	{
		IMoneyTransferPayeeRepositoryAdapter repositoryAdapter = (IMoneyTransferPayeeRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("MONEY_TRANSFER_PAYEE_REPOSITORY_ADAPTER");
		return repositoryAdapter.listAll(partyId, transferPayeeType, nickname);
	}
	public List<CzMoneyTransferPayee> listByPayeeType(PayeeKey key, String groupId, String nickName) throws Exception {
		IMoneyTransferPayeeRepositoryAdapter repositoryAdapter = (IMoneyTransferPayeeRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("MONEY_TRANSFER_PAYEE_REPOSITORY_ADAPTER");
		return repositoryAdapter.list(key, groupId, nickName);
	}
}
