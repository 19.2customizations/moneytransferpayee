package com.ecobank.digx.cz.app.moneytransfer.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ecobank.digx.cz.app.domain.moneytransfer.entity.payee.CzMoneyTransferPayee;
import com.ecobank.digx.cz.app.domain.moneytransfer.entity.policy.MoneyTransferPayeeBusinessPolicyData;
import com.ecobank.digx.cz.app.moneytransfer.assembler.MoneyTransferPayeeAssembler;
import com.ecobank.digx.cz.app.moneytransfer.dto.MTransferPayeeUpdateStatusRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MTransferPayeeUpdateStatusResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeListRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeListResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.PayeeStatusRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.PayeeStatusResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.ResponseDTO;
import com.ecobank.digx.cz.app.moneytransfer.service.ext.IMoneyTransferPayeeExtExecutor;
import com.ofss.digx.app.AbstractApplication;
import com.ofss.digx.app.Interaction;
import com.ofss.digx.app.payment.service.payee.InternalPayee;
import com.ofss.digx.domain.payment.entity.payee.PayeeGroup;
import com.ofss.digx.domain.payment.entity.payee.PayeeGroupKey;
import com.ofss.digx.domain.payment.entity.payee.PayeeKey;
import com.ofss.digx.enumeration.payment.payee.PayeeStatusType;
import com.ofss.digx.framework.domain.business.policy.factory.BusinessPolicyFactory;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.app.context.SessionContext;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.enumeration.EntityStatus;
import com.ofss.fc.framework.domain.policy.AbstractBusinessPolicy;
import com.ofss.fc.framework.domain.policy.IBusinessPolicyDTO;
import com.ofss.fc.infra.exception.RunTimeException;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.service.response.TransactionStatus;
import com.thoughtworks.xstream.XStream;

public class MoneyTransferPayeeService extends AbstractApplication implements IMoneyTransferPayee {

	private static final String THIS_COMPONENT_NAME = MoneyTransferPayeeService.class.getName();

	private MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	private transient Logger Logger = this.formatter.getLogger(THIS_COMPONENT_NAME);
	private IMoneyTransferPayeeExtExecutor extensionExecutor = null;

	public MoneyTransferPayeeService() {
//		this.extensionExecutor = (IMoneyTransferPayeeExtExecutor) ServiceExtensionFactory
//				.getServiceExtensionExecutor(MoneyTransferPayeeService.class.getName());
	}
	
	
	@Override
	public ResponseDTO create(SessionContext sessionContext,MoneyTransferPayeeRequestDTO requestDTO) throws Exception {
		
		System.out.println("CREATE-MONEY-TRANSFER-PAYEE" );
		
		/*String sAcc = rtSendMoneyCreateRequest.getSourceAccountId().getValue();
		System.out.println("Source Account: " + sAcc);
		// C35@~0022069204
		int x = sAcc.indexOf("~");
		String sourceAccount = sAcc.substring(x + 1);
		rtSendMoneyCreateRequest.setSourceAccountNo(sourceAccount);
		*/
		
		
		
		
	    XStream xs = new XStream();
//		
//		
		System.out.println("CREATE-MONEY_PAYEE" + xs.toXML(requestDTO));
//		
		
		
		if (this.Logger.isLoggable(Level.FINE))
			this.Logger.log(Level.FINE, this.formatter.formatMessage(
					"Entered into create method of Money Transfer payee: %s in class '%s'",
					new Object[] { requestDTO, THIS_COMPONENT_NAME }));
		/*checkAccessPolicy(
				"com.ofss.digx.cz.eco.app.moneytransfer.service.rapidtransfer.sendmoney.RapidTransferSendMoney.create",
				new Object[] { sessionContext, rtSendMoneyCreateRequest });
		canonicalizeInput(rtSendMoneyCreateRequest);*/
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		ResponseDTO responseDTO = new ResponseDTO();
		//AbstractBusinessPolicy abstractBusinessPolicy = null;
		//CreateRTSendMoneyBusinessPolicyData rtSendMoneyBusinessPolicyData = null;
		//AbstractBusinessPolicyFactory businessPolicyFactory = null;
		
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		
		BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();
		try {
			requestDTO.validate(sessionContext);
			
			String partyId = sessionContext.getTransactingPartyCode();
			String username = sessionContext.getUserId();
			System.out.println("Party Id in Service : " + partyId);
			
			//Check Duplicate Request -  party Id, PayeeTransferType , Receive Mode, Account No or Name
			
			CzMoneyTransferPayee payeeDomain = new CzMoneyTransferPayee();
			
			PayeeKey pKey = new PayeeKey();
			pKey.setPartyId(partyId);
			System.out.println("Party Id Nickname in Service : " + requestDTO.getPayeeDTO().getNickName());
			
			boolean isDuplicated = payeeDomain.payeeDuplicateCheck(partyId, requestDTO.getPayeeDTO().getReceiveMode(), requestDTO.getPayeeDTO().getPayeeTransferType(), requestDTO.getPayeeDTO().getNickName(), requestDTO.getPayeeDTO().getAccountId());
			System.out.println("Duplicate Check : " + isDuplicated);
			if(isDuplicated)
			{
				System.out.println("Duplicate Check : " + isDuplicated);
				List<CzMoneyTransferPayee> listP = payeeDomain.list(pKey, "", requestDTO.getPayeeDTO().getNickName() );
				if(listP != null && listP.size() > 0)
				{
					responseDTO.setPayeeId(listP.get(0).getKey().getId());	
				}
				System.out.println("Duplicate Check-22 : " + responseDTO.getPayeeId());
				responseDTO.setResponseCode("000");
				responseDTO.setResponseMessage("SUCCESSFUL");
				responseDTO.setStatus(buildStatus(status));
				return responseDTO;
			}
			MoneyTransferPayeeAssembler assembler = new MoneyTransferPayeeAssembler();
			
			MoneyTransferPayeeBusinessPolicyData policyData = assembler.toPolicyDataFromDTO(partyId, requestDTO.getPayeeDTO());

			abstractBusinessPolicy = businessPolicyFactory.getBusinesPolicyInstance("com.ecobank.digx.cz.app.moneytransfer.service.MoneyTransferPayeeService.create", (IBusinessPolicyDTO)policyData);
		    abstractBusinessPolicy.validate("DIGX_PY_CZ_E005");
			
			
			CzMoneyTransferPayee payeeObject = assembler.toDomainObject(requestDTO.getPayeeDTO(), username);
			payeeObject.setStatus(PayeeStatusType.PENDING);
			//payeeObject.setGroup(null);
			
			System.out.println("Duplicate Check : " + xs.toXML(payeeObject));
			String refx = GetRefNumber("", 10);
			String refNo = "MPY" + refx;
			
			PayeeKey key = new PayeeKey();
			key.setId(refNo);
			key.setPartyId(partyId);
			payeeObject.setKey(key);
			
			
			PayeeGroup group = new PayeeGroup();
			PayeeGroupKey gKey =   new PayeeGroupKey();  //this.generatePayeeGroupId();
			gKey.setGroupId( "MPYG" + refx);
			
			group.setKey(gKey);
			group.setName(requestDTO.getPayeeDTO().getNickName());
			group.setPartyId(partyId);
			group.setCreatedBy(username);
			group.setCreationDate(new Date());
			group.setContentId("");
			payeeObject.setGroup(group);
			group.create(group); 
			
			
			payeeObject.setCreatedBy(username);
			payeeObject.setCreationDate(new Date());
			payeeObject.setEntityStatus(EntityStatus.ACTIVE);
			payeeObject.setIsShared(true);
			
			System.out.println("Duplicate Check : " + xs.toXML(payeeObject));
			
			payeeObject.create(payeeObject);
			
			//System.out.println("Duplicate Check : " + xs.toXML(payeeObject));
			
			
			
			responseDTO.setPayeeId(refNo);
			responseDTO.setResponseCode("000");
			responseDTO.setResponseMessage("SUCCESSFUL");
			responseDTO.setStatus(buildStatus(status));
			
			
			//Adapter call to Remote Adapter ESB API that will RT - 
			
			//RemoteRTFeeRepositoryAdapter rtFeeAdapter = new RemoteRTFeeRepositoryAdapter();
			//rtFeeAdapter.fetchDetails(domain)
			//RTFeeAdapter rtAdapter = new RTFeeAdapter();
			//rtAdapter.fetchDetails(rtSendMoneyCreateRequest.getRtFeeDetails());
			//rtAdapter.validate(requestDTO)
			//this.extensionExecutor.postCreate(sessionContext, rtSendMoneyCreateRequest, rtSendMoneyCreateResponseDTO);
		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.Logger.log(Level.SEVERE,
					this.formatter.formatMessage("Fatal Exception from create for Generate Card Service  Request '%s'",
							new Object[] { responseDTO }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.Logger.log(Level.SEVERE,
					this.formatter.formatMessage("RunTimeException from create for Generate Card Service Request '%s'",
							new Object[] { responseDTO }),
					rte);
		} catch (java.lang.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.Logger.log(Level.SEVERE,
					this.formatter.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { ResponseDTO.class.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		encodeOutput(responseDTO);
		//checkResponsePolicy(sessionContext, rtSendMoneyCreateResponseDTO);
		/*if (this.logger.isLoggable(Level.FINE))
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of WesternUnionSendMoney Service, SessionContext: %s, WUSendMoneyCreateResponseDTO: %s ",
					new Object[] { sessionContext, rtSendMoneyCreateResponseDTO, THIS_COMPONENT_NAME }));
		*/
		return responseDTO;
	}

	@Override
	public MoneyTransferPayeeListResponse list(SessionContext sessionContext,
			MoneyTransferPayeeListRequestDTO payeeListRequest) throws Exception {
//		checkAccessPolicy("com.ecobank.digx.cz.app.rapidtransfer.service.payee.MoneyTransferPayeeService.list",
//				new Object[] { sessionContext, payeeListRequest });
		canonicalizeInput(payeeListRequest);
		if (this.Logger.isLoggable(Level.FINE))
			this.Logger.log(Level.FINE,
					this.formatter.formatMessage("Entering list, SessionContext: %s , PayeeListRequestDTO: %s ",
							new Object[] { sessionContext, payeeListRequest }));
		TransactionStatus transactionStatus = fetchTransactionStatus();
		MoneyTransferPayeeListResponse payeeListResponse = new MoneyTransferPayeeListResponse();
		Interaction.begin(sessionContext);
//		BusinessPolicyFactoryConfigurator businessPolicyFactoryConfigurator = BusinessPolicyFactoryConfigurator
//				.getInstance();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		//ListMoneyTransferPayeeBusinessPolicyData listPayeeBusinessPolicyData = null;
		try {
			payeeListRequest.validate(sessionContext);
//			this.extensionExecutor.preList(sessionContext, payeeListRequest);
			//listPayeeBusinessPolicyData = new ListMoneyTransferPayeeBusinessPolicyData();
//			listPayeeBusinessPolicyData.setMoneyTransferPayeeListRequestDTO(payeeListRequest);
//			AbstractBusinessPolicyFactory businessPolicyFactory = businessPolicyFactoryConfigurator
//					.getBusinessPolicyFactory("LIST_MONEYTRANSFER_PAYEE_POLICY_FACTORY");
//			abstractBusinessPolicy = businessPolicyFactory.getBusinesPolicyInstance("LIST_MONEYTRANSFER_PAYEE_POLICY",
//					(IBusinessPolicyDTO) listPayeeBusinessPolicyData);
//			abstractBusinessPolicy.validate();
			List<MoneyTransferPayeeDTO> listPayees = new ArrayList<>();
			//PayeeKey payeeKey = new MoneyTransferPayeeKey();
			//MoneyTransferPayeeDTO payeeDTO = new MoneyTransferPayeeDTO();
			//payeeKey.setPartyId(sessionContext.getTransactingPartyCode());
			MoneyTransferPayeeAssembler assembler = new MoneyTransferPayeeAssembler();
			CzMoneyTransferPayee payeeDomain = new CzMoneyTransferPayee();
			String partyId = sessionContext.getTransactingPartyCode();
			String transferPayeeType = payeeListRequest.getPayeeTransferType();
			String name = payeeListRequest.getNickName();
			List<CzMoneyTransferPayee> payeeList = payeeDomain.listAll(partyId, transferPayeeType, name);
			if(payeeList != null && payeeList.size() > 0)
			{
				for(CzMoneyTransferPayee czPayeeDomain : payeeList)
				{
					MoneyTransferPayeeDTO payeeDTO = assembler.fromDomainObject(czPayeeDomain);
					listPayees.add(payeeDTO);
				}
			}
			
			payeeListResponse.setListPayees(listPayees);
//			this.extensionExecutor.postList(sessionContext, payeeListRequest, payeeListResponse);
			payeeListResponse.setStatus(buildStatus(transactionStatus));
		} catch (Exception e1) {
			this.Logger.log(Level.SEVERE,
					this.formatter.formatMessage(
							"Exception encountered while invoking the service %s while listing payee.",
							new Object[] { THIS_COMPONENT_NAME }),
					(Throwable) e1);
			fillTransactionStatus(transactionStatus, e1);
		} catch (RunTimeException rte) {
			fillTransactionStatus(transactionStatus, rte);
			this.Logger.log(Level.SEVERE,
					this.formatter.formatMessage("RunTimeException from MoneyTransferPayee#list()", new Object[0]),
					(Throwable) rte);
		} catch (java.lang.Exception e1) {
			this.Logger.log(Level.SEVERE,
					this.formatter.formatMessage(
							"Exception encountered while invoking the service %s while listing payee.",
							new Object[] { THIS_COMPONENT_NAME }),
					e1);
			fillTransactionStatus(transactionStatus, e1);
		} finally {
			Interaction.close();
		}
		if (!payeeListRequest.isMTransferPayeeGroupRequest())
			encodeOutput(payeeListResponse);
//		checkResponsePolicy(sessionContext, payeeListResponse);
		if (this.Logger.isLoggable(Level.FINE))
			this.Logger.log(Level.FINE,
					this.formatter.formatMessage("Exiting list service %s", new Object[] { THIS_COMPONENT_NAME }));
		return payeeListResponse;
	}

	

	@Override
	public MTransferPayeeUpdateStatusResponse updateStatus(SessionContext sessionContext,
			MTransferPayeeUpdateStatusRequestDTO requestDto) throws Exception {
		super.checkAccessPolicy("com.ecobank.digx.cz.app.moneytransfer.service.MoneyTransferPayeeService.updateStatus",
				new Object[] { sessionContext, requestDto });
		if (this.Logger.isLoggable(Level.FINE))
			this.Logger.log(Level.FINE, this.formatter.formatMessage(
					"Entered into updateStatus of payee management.  Input: MTransferPayeeUpdateStatusRequestDTO: %s in class '%s'",
					new Object[] { "requestDto", THIS_COMPONENT_NAME }));
		super.canonicalizeInput(requestDto);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		MTransferPayeeUpdateStatusResponse response = new MTransferPayeeUpdateStatusResponse();
		//AbstractBusinessPolicy abstractBusinessPolicy = null;
		//UpdateStatusMTransferPayeeBusinessPolicyData updatePayeeStatusBusinessPolicyData = null;
//		BusinessPolicyFactoryConfigurator businessPolicyFactoryConfigurator = BusinessPolicyFactoryConfigurator
//				.getInstance();
		try {
		System.out.println("Money Transfer Remittance Payee Update " + requestDto.getPayeeId());
		
		requestDto.validate(sessionContext);
		
			CzMoneyTransferPayee payee = null;
			
			CzMoneyTransferPayee rtPayeedomain = new CzMoneyTransferPayee();
			
			String payeeId = requestDto.getPayeeId();
			String partyId = sessionContext.getTransactingPartyCode();
			
			
			PayeeKey payeeKey = new PayeeKey();
			payeeKey.setId(payeeId);
			payeeKey.setPartyId(partyId);
			
			payee = rtPayeedomain.read(payeeKey);
			if(payee != null)
			{
				System.out.println("Money Transfer Remittance Payee Update Found " + payee.getNickName());
				payee.setStatus(PayeeStatusType.ACTIVE);
				rtPayeedomain.update(payee);
			}
			
			
			response.setStatus(buildStatus(status));
//			this.extensionExecutor.postUpdateStatus(sessionContext, requestDto, response);
		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);

		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);

		} /*catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);

		}*/ finally {
			Interaction.close();
		}
		encodeOutput(response);
//		checkResponsePolicy(sessionContext, response);
		if (this.Logger.isLoggable(Level.FINE))
			this.Logger.log(Level.FINE,
					this.formatter.formatMessage(
							"Exiting updateStatus of Payee, SessionContext: %s, TransactionStatus: %s in class '%s' ",
							new Object[] { sessionContext, response, THIS_COMPONENT_NAME }));
		return response;
	}
	
	@Override
	public PayeeStatusResponse delete(SessionContext sessionContext, PayeeStatusRequestDTO requestDTO) throws Exception {
		if (this.Logger.isLoggable(Level.FINE))
			this.Logger.log(Level.FINE, this.formatter.formatMessage(
					"Entered into delete of money transfer remittance payee service.  Input: PayeeStatusRequestDTO: %s in class '%s'",
					new Object[] { requestDTO, THIS_COMPONENT_NAME }));
//		checkAccessPolicy("com.ecobank.digx.cz.app.rapidtransfer.service.payee.RapidTransferPayeeService.delete",
//				new Object[] { sessionContext, payeeDeleteRequest });
		canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = fetchTransactionStatus();
		PayeeStatusResponse response = new PayeeStatusResponse();
		PayeeKey key = new PayeeKey();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
//		BusinessPolicyFactoryConfigurator businessPolicyFactoryConfigurator = BusinessPolicyFactoryConfigurator
//				.getInstance();
		try {
			requestDTO.validate(sessionContext);
			CzMoneyTransferPayee payee = null;
			
			CzMoneyTransferPayee rtPayeedomain = new CzMoneyTransferPayee();
			
			String payeeId = requestDTO.getPayeeId();
			String partyId = sessionContext.getTransactingPartyCode();
			
			
			PayeeKey payeeKey = new PayeeKey();
			payeeKey.setId(payeeId);
			payeeKey.setPartyId(partyId);
			
			payee = rtPayeedomain.read(payeeKey);
			if(payee != null)
			{
				System.out.println("Money Transfer Remittance Payee Delete Found " + payee.getNickName());
				payee.setStatus(PayeeStatusType.ACTIVE);
				rtPayeedomain.delete(payee);
			}
			
			response.setStatus(buildStatus(transactionStatus));
			
//			
		} catch (Exception e1) {
			this.Logger.log(Level.SEVERE,
					this.formatter.formatMessage(
							"Exception encountered while invoking the service %s while deleting rapid transfer payee",
							new Object[] { InternalPayee.class.getName() }),
					(Throwable) e1);
			fillTransactionStatus(transactionStatus, e1);
		} catch (RuntimeException rte) {
			fillTransactionStatus(transactionStatus, rte);
			this.Logger.log(Level.SEVERE,
					this.formatter.formatMessage("RuntimeException from delete for RapidTransferPayeeRequestDTO '%s'",
							new Object[] { requestDTO }),
					rte);
		} finally {
			Interaction.close();
		}
 		checkResponsePolicy(sessionContext, response);
		encodeOutput(response);
		if (this.Logger.isLoggable(Level.FINE))
			this.Logger.log(Level.FINE,
					this.formatter.formatMessage(
							"Exiting delete of Money Transfer Payee Remittance, SessionContext: %s, TransactionStatus: %s in class '%s' ",
							new Object[] { sessionContext, response, THIS_COMPONENT_NAME }));
		return response;
	}
	
	
	public void savePayee(String partyId, CzMoneyTransferPayee payeeDomain,String username) throws Exception
	{
		
		CzMoneyTransferPayee rtPayeedomain =  null;
		
		if(rtPayeedomain == null)
		{
			
			XStream xs = new XStream();
			System.out.println("Money Domain: " +  xs.toXML(payeeDomain));
			//String payoutCcy = rtSendMoneyDomain.getPayeeDetails().getCcyCode();
			
			
			//For Existing customer account, customer Id of beneficiary account will be partyId
			
			PayeeGroup group = new PayeeGroup();
			PayeeGroupKey gKey =   new PayeeGroupKey();  //this.generatePayeeGroupId();
			gKey.setGroupId( GetRefNumber("MPYG", 9));
			
			
			
			group.setKey(gKey);
			group.setName(payeeDomain.getNickName());
			group.setPartyId(partyId);
			group.setCreatedBy(username);
			group.setCreationDate(new Date());
			group.setContentId("");
			
			payeeDomain.setGroup(group);
			group.create(group); //Insert into DIGX_PY_PAYEEGROUP
			
			PayeeKey payeeKey = new PayeeKey();
			payeeKey.setId(GetRefNumber("PYE", 12));
			payeeKey.setPartyId(partyId);
			
			
			
			System.out.println("Payee Group Save Done ");
			System.out.println("Payee Save Done " + xs.toXML(group));
			
			/*payeeDomain.setGroup(group);
			rtPayeeDomain.setKey(payeeKey);
			System.out.println("Payee Save Done " + xs.toXML(rtPayeeDomain));
			rtPayeeDomain.create(rtPayeeDomain); //DIGX_PY_PAYEE & DIGX_CZ_RT_PAYEE
			System.out.println("Payee Save Done ");*/
			
		}
	}
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }

}
