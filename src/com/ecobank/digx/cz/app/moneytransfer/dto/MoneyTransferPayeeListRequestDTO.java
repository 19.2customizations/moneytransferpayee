package com.ecobank.digx.cz.app.moneytransfer.dto;

import java.util.List;

import com.ecobank.digx.cz.app.enumeration.moneytransfer.payee.MoneyTransferPayeeType;
import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.enumeration.payment.payee.PayeeStatusType;

public class MoneyTransferPayeeListRequestDTO extends DataTransferObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2066010737737551945L;

	private String nickName;

	private String partyId;
	
	private String payeeTransferType;

	private List<MoneyTransferPayeeType> types = null;

	private String groupId;

	private boolean mTransferPayeeGroupRequest;

	private boolean isExpandAll;

	private Boolean isShared;

	private List<PayeeStatusType> statusTypes = null;

	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public List<MoneyTransferPayeeType> getTypes() {
		return this.types;
	}

	public void setTypes(List<MoneyTransferPayeeType> types) {
		this.types = types;
	}

	public String getGroupId() {
		return this.groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public boolean isExpandAll() {
		return this.isExpandAll;
	}

	public void setExpandAll(boolean isExpandAll) {
		this.isExpandAll = isExpandAll;
	}

	public boolean isMTransferPayeeGroupRequest() {
		return this.mTransferPayeeGroupRequest;
	}

	public void setMTransferPayeeGroupRequest(boolean payeeGroupRequest) {
		this.mTransferPayeeGroupRequest = payeeGroupRequest;
	}

	public Boolean getIsShared() {
		return this.isShared;
	}

	public void setIsShared(Boolean isShared) {
		this.isShared = isShared;
	}

	public List<PayeeStatusType> getStatusTypes() {
		return this.statusTypes;
	}

	public void setStatusTypes(List<PayeeStatusType> statusTypes) {
		this.statusTypes = statusTypes;
	}

	public String getPartyId() {
		return this.partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getPayeeTransferType() {
		return payeeTransferType;
	}

	public void setPayeeTransferType(String payeeTransferType) {
		this.payeeTransferType = payeeTransferType;
	}

}
