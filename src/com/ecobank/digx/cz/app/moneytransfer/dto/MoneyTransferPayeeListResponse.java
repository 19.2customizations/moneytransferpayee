package com.ecobank.digx.cz.app.moneytransfer.dto;

import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class MoneyTransferPayeeListResponse extends BaseResponseObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8018352556856955648L;

	private List<MoneyTransferPayeeDTO> listPayees;

	public List<MoneyTransferPayeeDTO> getListPayees() {
		return this.listPayees;
	}

	public void setListPayees(List<MoneyTransferPayeeDTO> listPayees) {
		this.listPayees = listPayees;
	}

}
