package com.ecobank.digx.cz.app.moneytransfer.service.ext;

import com.ecobank.digx.cz.app.moneytransfer.dto.MTransferPayeeUpdateStatusRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MTransferPayeeUpdateStatusResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeListRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeListResponse;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.app.context.SessionContext;

public class VoidMoneyTransferPayeeExt implements IMoneyTransferPayeeExt {

	@Override
	public void preList(SessionContext paramSessionContext,
			MoneyTransferPayeeListRequestDTO paramMoneyTransferPayeeListRequestDTO) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void postList(SessionContext paramSessionContext,
			MoneyTransferPayeeListRequestDTO paramMoneyTransferPayeeListRequestDTO,
			MoneyTransferPayeeListResponse paramMoneyTransferPayeeListResponse) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void preUpdateStatus(SessionContext paramSessionContext,
			MTransferPayeeUpdateStatusRequestDTO paramMTransferPayeeUpdateStatusRequestDTO) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void postUpdateStatus(SessionContext paramSessionContext,
			MTransferPayeeUpdateStatusRequestDTO paramMTransferPayeeUpdateStatusRequestDTO,
			MTransferPayeeUpdateStatusResponse paramMTransferPayeeUpdateStatusResponse) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
