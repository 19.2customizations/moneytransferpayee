package com.ecobank.digx.cz.app.moneytransfer.service.ext;

import java.util.List;

import com.ecobank.digx.cz.app.moneytransfer.dto.MTransferPayeeUpdateStatusRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MTransferPayeeUpdateStatusResponse;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeListRequestDTO;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeListResponse;
import com.ecobank.digx.cz.app.moneytransfer.service.MoneyTransferPayeeService;
import com.ofss.digx.app.ext.ServiceExtensionFactory;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.app.context.SessionContext;

public class MoneyTransferPayeeExtExecutor implements IMoneyTransferPayeeExtExecutor {

	private List<IMoneyTransferPayeeExt> extensions = ServiceExtensionFactory
			.getServiceExtensions(MoneyTransferPayeeService.class.getName());

	@Override
	public void preList(SessionContext sessionContext, MoneyTransferPayeeListRequestDTO payeeListRequestDTO)
			throws Exception {
		for (IMoneyTransferPayeeExt extension : this.extensions)
			extension.preList(sessionContext, payeeListRequestDTO);

	}

	@Override
	public void postList(SessionContext sessionContext, MoneyTransferPayeeListRequestDTO payeeListRequestDTO,
			MoneyTransferPayeeListResponse response) throws Exception {
		for (IMoneyTransferPayeeExt extension : this.extensions)
			extension.postList(sessionContext, payeeListRequestDTO, response);

	}

	@Override
	public void preUpdateStatus(SessionContext sessionContext,
			MTransferPayeeUpdateStatusRequestDTO payeeUpdateStatusRequestDTO) throws Exception {
		for (IMoneyTransferPayeeExt extension : this.extensions)
			extension.preUpdateStatus(sessionContext, payeeUpdateStatusRequestDTO);
	}

	@Override
	public void postUpdateStatus(SessionContext sessionContext,
			MTransferPayeeUpdateStatusRequestDTO payeeUpdateStatusRequestDTO,
			MTransferPayeeUpdateStatusResponse response) throws Exception {
		for (IMoneyTransferPayeeExt extension : this.extensions)
			extension.postUpdateStatus(sessionContext, payeeUpdateStatusRequestDTO, response);

	}

}
