package com.ecobank.digx.cz.appx.moneytransfer.service;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeRequestDTO;

public interface IMoneyTransferPayee {

	
	 Response create(MoneyTransferPayeeRequestDTO requestDTO);
	
	Response list(String partyId, String payeeTransferType);

	Response updateStatus(String payeeId);
	 Response delete(@PathParam("payeeId") String payeeId);

}
