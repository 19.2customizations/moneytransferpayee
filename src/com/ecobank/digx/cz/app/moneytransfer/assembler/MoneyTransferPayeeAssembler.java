package com.ecobank.digx.cz.app.moneytransfer.assembler;


import java.util.logging.Level;
import java.util.logging.Logger;

import com.ecobank.digx.cz.app.domain.moneytransfer.entity.payee.CzMoneyTransferPayee;
import com.ecobank.digx.cz.app.domain.moneytransfer.entity.payee.MoneyTransferPayeeDetails;
import com.ecobank.digx.cz.app.domain.moneytransfer.entity.policy.MoneyTransferPayeeBusinessPolicyData;
import com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeDTO;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.framework.domain.IAbstractDomainObject;
import com.ofss.fc.framework.domain.assembler.AbstractAssembler;
import com.ofss.fc.framework.domain.common.dto.DataTransferObject;
import com.ofss.fc.framework.domain.common.dto.DomainObjectDTO;
import com.ofss.fc.infra.exception.FatalException;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class MoneyTransferPayeeAssembler extends AbstractAssembler {
	
	private static final String THIS_COMPONENT_NAME = MoneyTransferPayeeAssembler.class.getName();
	private MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	private transient Logger logger = this.formatter.getLogger(THIS_COMPONENT_NAME);

	@Override
	public DomainObjectDTO fromDomainObject(IAbstractDomainObject arg0) throws FatalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IAbstractDomainObject toDomainObject(DomainObjectDTO arg0) throws FatalException {
		// TODO Auto-generated method stub
		return null;
	}

	public MoneyTransferPayeeDTO fromDomainObject(CzMoneyTransferPayee payeeDomain) throws Exception {
		MoneyTransferPayeeDTO payeeDTO = new MoneyTransferPayeeDTO();
		if (payeeDomain != null) {
			populateAbstractDomainObjectMap("com.ecobank.digx.cz.app.domain.moneytransfer.entity.payee.CzMoneyTransferPayee",
					(IAbstractDomainObject) payeeDomain);
			payeeDTO.setId(payeeDomain.getKey().getId());
			payeeDTO.setPartyId(payeeDomain.getKey().getPartyId());
			payeeDTO.setGroupId(payeeDomain.getGroup().getKey().getGroupId());
			payeeDTO.setGroupName(payeeDomain.getGroup().getName());
			payeeDTO.setIsShared(payeeDomain.getIsShared());
			payeeDTO.setNickName(payeeDomain.getNickName());
			payeeDTO.setPayeeTransferType(payeeDomain.getMoneyTransferPayeeDetails().getPayeeType());
			payeeDTO.setAccountId(payeeDomain.getMoneyTransferPayeeDetails().getAccountId());
			payeeDTO.setAccountName(payeeDomain.getMoneyTransferPayeeDetails().getAccountName());
			payeeDTO.setBankCode(payeeDomain.getMoneyTransferPayeeDetails().getBankCode());
			payeeDTO.setIdType(payeeDomain.getMoneyTransferPayeeDetails().getIdType());
			payeeDTO.setIdNumber(payeeDomain.getMoneyTransferPayeeDetails().getIdNumber());
			payeeDTO.setIdIssueDate(payeeDomain.getMoneyTransferPayeeDetails().getIdIssueDate());
			payeeDTO.setIdExpiryDate(payeeDomain.getMoneyTransferPayeeDetails().getIdExpiryDate());
			payeeDTO.setFirstName(payeeDomain.getMoneyTransferPayeeDetails().getFirstName());
			payeeDTO.setLastName(payeeDomain.getMoneyTransferPayeeDetails().getLastName());
			payeeDTO.setCountry(payeeDomain.getMoneyTransferPayeeDetails().getCountry());
			payeeDTO.setCcyCode(payeeDomain.getMoneyTransferPayeeDetails().getCcyCode());
			payeeDTO.setEmail(payeeDomain.getMoneyTransferPayeeDetails().getEmail());
			payeeDTO.setPhoneNo(payeeDomain.getMoneyTransferPayeeDetails().getPhoneNo());
			
			//payeeDTO.setPayeeType(payeeDomain.getType());
			payeeDTO.setStatus(payeeDomain.getStatus());
			//payeeDTO.setDictionaryArray(getDictionaryArray((IORMEntity) payeeDomain));
		}
		return payeeDTO;
	}
	
	public CzMoneyTransferPayee toDomainObject(MoneyTransferPayeeDTO requestDTO,String username) throws Exception {
		CzMoneyTransferPayee domainObj = new CzMoneyTransferPayee();
		if (requestDTO != null) {
			populateDataTransferObjectDTOMap("com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeDTO",
					(DataTransferObject) requestDTO);
			try {
				if (retrieveDataTransferObjectDTOMapElement(
						"com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeDTO")
								.getDictionaryArray() == null) {
					domainObj = new CzMoneyTransferPayee();
				} else {
					domainObj = (CzMoneyTransferPayee) getCustomizedDomainObject(retrieveDataTransferObjectDTOMapElement(
							"com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeDTO"));
				}
			} catch (Exception e) {
				this.logger.log(Level.WARNING,
						"Customized Domain Object failed to be instantiated from com.ecobank.digx.cz.app.moneytransfer.dto.MoneyTransferPayeeDTO",
						e);
				domainObj = new CzMoneyTransferPayee();
			}
			if (requestDTO != null) {
				
				MoneyTransferPayeeDetails payeeDetails = new MoneyTransferPayeeDetails();
				
				payeeDetails.setAccountName(requestDTO.getAccountName());
				payeeDetails.setAccountId(requestDTO.getAccountId());
				payeeDetails.setFirstName(requestDTO.getFirstName());
				payeeDetails.setLastName(requestDTO.getLastName());
				payeeDetails.setIdType(requestDTO.getIdType());
				payeeDetails.setIdNumber(requestDTO.getIdNumber());
				payeeDetails.setIdIssueDate(requestDTO.getIdIssueDate());
				payeeDetails.setIdExpiryDate(requestDTO.getIdExpiryDate());
				payeeDetails.setReceiveMode(requestDTO.getReceiveMode());
				payeeDetails.setCountry(requestDTO.getCountry());
				payeeDetails.setCcyCode(requestDTO.getCcyCode());
				payeeDetails.setAddressLine1(requestDTO.getAddressLine1());
				payeeDetails.setAddressLine2("");
				payeeDetails.setCity(requestDTO.getCity());
				payeeDetails.setState(requestDTO.getState());
				payeeDetails.setPayeeType(requestDTO.getPayeeTransferType());
				payeeDetails.setPhoneNo(requestDTO.getPhoneNo());
				payeeDetails.setEmail(requestDTO.getEmail());
				payeeDetails.setCreatedBy(username);
				payeeDetails.setCreationDate(new Date());
				payeeDetails.setLastUpdatedBy(username);
				payeeDetails.setLastUpdatedDate(new Date());
				domainObj.setMoneyTransferPayeeDetails(payeeDetails);
					
					
			}
			
			
			domainObj.setNickName(requestDTO.getNickName());
			
			
				
			
			//if (getVersion() != null && getVersion().length() > 0)
				//domainObj.setVersion(Integer.valueOf(Integer.parseInt(getVersion())));
		}
		return domainObj;
	}
	
	public MoneyTransferPayeeBusinessPolicyData toPolicyDataFromDTO(String partyId,MoneyTransferPayeeDTO requestDTO)
	{
		MoneyTransferPayeeBusinessPolicyData policyData = new MoneyTransferPayeeBusinessPolicyData();
		try
		{
			policyData.setAccountNo(requestDTO.getAccountId());
			policyData.setNickname(requestDTO.getNickName());
			policyData.setPartyId(partyId);
			policyData.setReceiveMode(requestDTO.getReceiveMode());
			policyData.setTransferPayeeType(requestDTO.getPayeeTransferType());
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return policyData;
	}

}
